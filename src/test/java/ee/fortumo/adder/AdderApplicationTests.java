package ee.fortumo.adder;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AdderApplicationTests {

	@LocalServerPort
	private int port;

	private final AsyncRestTemplate asyncRestTemplate =  new AsyncRestTemplate();

	@Test
	public void shouldRespondWithCorrectSum() throws Exception {
		List<ListenableFuture<ResponseEntity<Integer>>> addResponses = new ArrayList<>();
		for (int i = 1; i < 20; i++) {
			addResponses.add(getFutureResponse(String.valueOf(i)));
		}
		Thread.sleep(500);
		ListenableFuture<ResponseEntity<Integer>> total = getFutureResponse("end");

		assertEquals(190, addResponses.get(0).get().getBody());
		assertEquals(190, addResponses.get(1).get().getBody());
		assertEquals(190, addResponses.get(2).get().getBody());
		assertEquals(190, addResponses.get(3).get().getBody());
		assertEquals(190, addResponses.get(4).get().getBody());
		assertEquals(190, addResponses.get(5).get().getBody());
		assertEquals(190, addResponses.get(6).get().getBody());
		assertEquals(190, addResponses.get(7).get().getBody());
		assertEquals(190, addResponses.get(8).get().getBody());
		assertEquals(190, addResponses.get(9).get().getBody());
		assertEquals(190, addResponses.get(10).get().getBody());
		assertEquals(190, addResponses.get(11).get().getBody());
		assertEquals(190, addResponses.get(12).get().getBody());
		assertEquals(190, addResponses.get(13).get().getBody());
		assertEquals(190, addResponses.get(14).get().getBody());
		assertEquals(190, addResponses.get(15).get().getBody());
		assertEquals(190, addResponses.get(16).get().getBody());
		assertEquals(190, addResponses.get(17).get().getBody());
		assertEquals(190, addResponses.get(18).get().getBody());
		assertEquals(190, total.get().getBody());
	}

	@Test
	public void shouldRespondWithZeroIfOnlyEndCommand() throws Exception {
		ListenableFuture<ResponseEntity<Integer>> total = getFutureResponse("end");

		assertEquals(0, total.get().getBody());
	}

	@Test
	public void shouldRespondWithCorrectSumAfterEndCommand() throws Exception {
		ListenableFuture<ResponseEntity<Integer>> total1 = getFutureResponse("end");
		Thread.sleep(500);
		ListenableFuture<ResponseEntity<Integer>> add1 = getFutureResponse("1");
		ListenableFuture<ResponseEntity<Integer>> add2 = getFutureResponse("2");
		ListenableFuture<ResponseEntity<Integer>> total2 = getFutureResponse("end");

		assertEquals(0, total1.get().getBody());
		assertEquals(3, add1.get().getBody());
		assertEquals(3, add2.get().getBody());
		assertEquals(3, total2.get().getBody());
	}

	@Test
	public void shouldThrowExceptionIfInputIsInvalid() {
		ListenableFuture<ResponseEntity<Integer>> input = getFutureResponse("hello");

		assertThrows(ExecutionException.class, input::get);
	}

	private ListenableFuture<ResponseEntity<Integer>> getFutureResponse(String body) {
		final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
		String url = "http://localhost:" + port + "/";
		Map<String, String> map = new HashMap<>();
		return asyncRestTemplate.postForEntity(url, new HttpEntity<>(body, headers), Integer.class, map);
	}

}
