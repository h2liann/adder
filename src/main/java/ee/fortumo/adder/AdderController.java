package ee.fortumo.adder;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AdderController {

    private final ThreadPoolTaskExecutor threadPool;
    private static final AsyncSum ASYNC_SUM = new AsyncSum();

    @PostMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public Integer sum(@RequestBody String input) throws ExecutionException, InterruptedException {
        String cleanedInput = input.substring(0, input.length() - 1);
        log.info("User input is {} from thread {}", cleanedInput, Thread.currentThread().getName());
        Future<Integer> result = threadPool.submit(new AsyncCalculationCallable(ASYNC_SUM, cleanedInput));
        Integer sum = result.get();
        log.info("Total sum {} from thread {}", sum, Thread.currentThread().getName());
        ASYNC_SUM.reset();
        return sum;
    }
}
