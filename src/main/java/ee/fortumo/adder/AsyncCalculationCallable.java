package ee.fortumo.adder;

import java.util.concurrent.Callable;

public class AsyncCalculationCallable implements Callable<Integer> {

    private final AsyncSum asyncSum;
    private final String input;

    public AsyncCalculationCallable(AsyncSum asyncSum, String input) {
        this.asyncSum = asyncSum;
        this.input = input;
    }

    @Override
    public Integer call() {
        if ("end".equalsIgnoreCase(input)) {
            return asyncSum.getTotal();
        }
        try {
            return asyncSum.add(Integer.parseInt(input));
        } catch (NumberFormatException e) {
            throw new RuntimeException("Invalid input. Accepted inputs are integer numbers or end keyword");
        }

    }
}
