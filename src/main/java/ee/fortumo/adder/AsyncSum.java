package ee.fortumo.adder;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class AsyncSum {

    private final AtomicInteger sum = new AtomicInteger();
    private final AtomicInteger threadCount = new AtomicInteger();
    private final AtomicBoolean isDone = new AtomicBoolean();

    public synchronized Integer add(int value) {
        while (!isDone.get()) {
            try {
                addAtomic(sum, value);
                addAtomic(threadCount, 1);
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                log.error("InterruptedException");
            }
        }
        return sum.get();
    }

    public synchronized Integer getTotal() {
        isDone.set(true);
        addAtomic(threadCount, 1);
        notifyAll();
        return sum.get();
    }

    public void reset() {
        decreaseCounter();
        log.info("Thread counter decreased to " + threadCount.get() + " by thread " + Thread.currentThread().getName());
        if (threadCount.get() == 0) {
            isDone.set(false);
            sum.set(0);
        }
    }

    private void addAtomic(AtomicInteger atomic, int value) {
        while (true) {
            int existing = atomic.get();
            int newValue = existing + value;
            if (atomic.compareAndSet(existing, newValue)) {
                return;
            }
        }
    }

    private void decreaseCounter() {
        while (true) {
            int existing = threadCount.get();
            int newValue = existing - 1;
            if (threadCount.compareAndSet(existing, newValue)) {
                return;
            }
        }
    }

}
