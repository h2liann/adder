# Adder

A simple web server that can sum up numbers in a multi-threaded environment.

### Build

```bash
./gradlew build 
```
### Deploy



```bash
java -jar adder-1.0.0.jar
```
### Usage

* to add int value
```bash
curl -d [int value] http://localhost:8080/
```

* to get sum
```bash
curl -d end http://localhost:8080/
```
